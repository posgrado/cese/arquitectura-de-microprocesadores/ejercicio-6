/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/30
 *===========================================================================*/

#ifndef __EJERCICIO_C_FUNCTION_H__
#define __EJERCICIO_C_FUNCTION_H__

#include "ejercicio.h"

/**
 * c_function do the same of ASM function for compare purpose
 * 
 * @param * vectorIn de 32 bits
 * @param * vectorOut vector de salida de 16 bits
 * @param longitud
 */
void c_pack32to16 (int32_t * vectorIn, int16_t *vectorOut, uint32_t longitud);

#endif /* __EJERCICIO_C_FUNCTION_H__ */
