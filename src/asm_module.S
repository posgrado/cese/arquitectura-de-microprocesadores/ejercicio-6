/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/30
 *===========================================================================*/

/**
 * Directiva al ensablador que permite indicar que se encarga de buscar
 * la instruccion mas apropiada entre ARM y thumb2
 */
.syntax unified

/**
 * .text permite indicar una seccion de codigo.
 */
.text

/**
 * .global permite definir un simbolo exportable,
 * es decir que podemos verlo desde otros modulos (equivalente a extern).
 * Definimos la rutina como global para que sea visible desde otros modulos.
 */
.global asm_pack32to16

/**
 * Indicamos que la siguiente subrutina debe ser ensamblada en modo thumb,
 * entonces en las direcciones en el ultimo bit tendran el 1 para que se reconozcan como en modo thumb.
 * Siempre hay que ponerla antes de la primer instruccion.
 */
.thumb_func


/*=====[Definition macros of public constants]===============================*/



#define vectorIn	r0
#define vectorOut	r1
#define longitud	r2

/*=====[Implementations of public assembly functions]=================================*/

/**
 *	prototipo de la funcion en C
 *
 *	void asm_pack32to16 (int32_t * vectorIn, int16_t *vectorOut, uint32_t longitud);
*/
asm_pack32to16:
    push {lr}  /* guardamos la direccion de retorno en la pila */
	subs longitud, 1 /* le resto 1 para empezar el loop y 10 para la ventana del filtro*/
	loop:
		ldr r3, [vectorIn, longitud, LSL 2]
		ssat r3, 16, r3
		strh r3, [vectorOut, longitud, LSL 1]
		subs longitud, 1
		bpl loop
	pop {pc}   /* retorno */

	/* otras alternativas para el retorno */
	/* 1. mov pc,lr
	/  2. bx lr */
	/* pop {pc} */
