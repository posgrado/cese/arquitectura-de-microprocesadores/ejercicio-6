/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/30
 *===========================================================================*/

#include "c_function.h"

void c_pack32to16(int32_t * vectorIn, int16_t *vectorOut, uint32_t longitud) {
	for (int i = 0; i < longitud; ++i) {
		if (vectorIn[i] < -32768){
			vectorOut[i] = -32768;
		} else if (vectorIn[i] > 32767){
			vectorOut[i] = 32767;
		} else {
			vectorOut[i] = (int16_t) vectorIn[i];
		}
	}
}

